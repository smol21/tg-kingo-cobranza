import Swal from "sweetalert2";
import { useLoading } from "vue-loading-overlay";

export const fireToastMsg = (msg, icon = "warning") => {
  return Swal.fire({
    position: "top-end",
    toast: true,
    icon,
    title: msg || "",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
  });
};

export const pre_loader = (hide = false) => {
      const $loading = useLoading();    
      let loader = $loading.show({
        loader: "dots",
        color: "blue",
        width: 100,
        height: 100,
      });
      setInterval(() => {
        if (hide){
          
          loader.hide();
        } 
      }, 3000);
      
      // return loader;
}

export const format_epoch = (date) => {
      const dia = date.getDate();
      const mes = date.getMonth() + 1;
      const anno = date.getFullYear();
      const fecha_actual = anno + '-' + mes + '-' + dia;
      const fechaActual_ = new Date(fecha_actual);
      return (fechaActual_.getTime() + 14400000) / 1000;
}

export const DateFormaterEpoch = (date , withSlash = false, enType = false ) =>  {
  const d = new Date(date * 1000);
  const date_ = d.getDate(), month_ = d.getMonth() + 1, year_ = d.getFullYear();
  const dateVal = `${date_}`.length > 1 ? date_ : `0${date_}`;
  const datestring = 
    (enType ? year_ : dateVal) + `${withSlash ? '/' : '-'}` + 
    (`${month_}`.length > 1 ? month_ : `0${month_}`) + `${withSlash ? '/' : '-'}` + (enType ? dateVal : year_);
  return `${datestring}`;
}

export const menuFormatter = (datos_) => {
  const menu = datos_.reduce((menu, item) => {
    if (+item.fatherCode == 0 && !menu[+item.menuCode])
      menu[+item.menuCode] = {
        ...item,
        menuCode: +item.menuCode,
        fatherCode: +item.fatherCode,
        menu: [],
      };
    else if (+item.fatherCode == 0 && menu[+item.menuCode])
      menu[+item.menuCode] = {
        ...menu[+item.menuCode],
        ...item,
        menuCode: +item.menuCode,
        fatherCode: +item.fatherCode,
      };
    else if (menu[+item.fatherCode])
      menu[+item.fatherCode].menu.push({
        ...item,
        menuCode: +item.menuCode,
        fatherCode: +item.fatherCode,
      });
    else
      menu[+item.fatherCode] = {
        menu: [
          {
            ...item,
            menuCode: +item.menuCode,
            fatherCode: +item.fatherCode,
          },
        ],
      };
    return menu;
  }, {});

  return menu;
}
