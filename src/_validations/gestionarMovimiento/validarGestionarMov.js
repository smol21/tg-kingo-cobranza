import { helpers,required } from "@vuelidate/validators";
import { notSeleccione} from "@/_validations/ValidacionEspeciales"; 

// export const validarGestionarMov = {
//     comprador: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
//     producto: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
//     sorteo: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
//     tipoMov: { required },
//     cuenta: { required },
//     banco_oficina: { required },
//     estatus: { required },
//     fecha_desde: { required },
//     fecha_hasta: { required },
// }

export const validarGestionarMov = (userType = '') => {
    return {
        ...(userType.value === '05' ? {} : { 
            comprador: {
            notSeleccione: helpers.withMessage("Campo requerido.", notSeleccione),
          }}),
      
      producto: {
        notSeleccione: helpers.withMessage("Campo requerido.", notSeleccione),
      },
      sorteo: {
        notSeleccione: helpers.withMessage("Campo requerido.", notSeleccione),
      },
      tipoMov: { required },
      cuenta: { required },
      banco_oficina: { required },
      estatus: { 
        notSeleccione: helpers.withMessage("Campo requerido.", notSeleccione), 
      },
      fecha_desde: { required },
      fecha_hasta: { required },
    };
}