import { required, maxLength, minLength, helpers } from "@vuelidate/validators";
import { passwordRgx, SoloAlfanumericos } from '@/_validations/ValidacionEspeciales';

export const validarLogin  = {
    login: { 
        required: helpers.withMessage('Campo requerido', required), 
        minLength:  helpers.withMessage('Debe tener entre 6 a 12 caracteres.', minLength(6)) ,
        maxLength:  helpers.withMessage('Debe tener entre 6 a 12 caracteres.', maxLength(12)) ,
        SoloAlfanumericos: helpers.withMessage('Sólo puede introducir valores alfanuméricos.', SoloAlfanumericos) ,
    },
    pass: { 
        required: helpers.withMessage('Campo requerido', required), 
        minLength:  helpers.withMessage('Debe tener entre 6 a 12 caracteres.', minLength(6)) ,
        maxLength:  helpers.withMessage('Debe tener entre 6 a 12 caracteres.', maxLength(12)) ,
        passwordRgx: helpers.withMessage('Debe tipear letras y/o números sin espacios en blanco.', passwordRgx) ,
    },  
}