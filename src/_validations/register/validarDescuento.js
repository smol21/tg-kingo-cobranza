import { helpers,required, maxLength, minLength } from "@vuelidate/validators";
import { notZeros,notSeleccione, SoloNumeros, numberAndDecimals,SoloAlfanumericos,SoloLetras /* onlyNumber_espec */ } from "@/_validations/ValidacionEspeciales"; 

export const validarDescuento  = {
    comprador: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
    producto: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
    sorteo: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
    fecha: { required },
    descripcion: { 
        required: helpers.withMessage('Campo requerido', required), 
        minLength:  helpers.withMessage('La longitud mínima permitida es de 4 caracteres.', minLength(4)),
        maxLength:  helpers.withMessage('La longitud máxima permitida es de 150 caracteres.', maxLength(250)) 
    },
    monto: { 
        required: helpers.withMessage('Campo requerido', required), 
        numberAndDecimals: helpers.withMessage('Solo puede introducir coma (,), seguido de máximo 2 dígitos. Ej: 0,00', numberAndDecimals), 
        maxLength:helpers.withMessage('La longitud máxima permitida es de 7 caracteres.', maxLength(7)) ,
        notZeros: helpers.withMessage('El monto no puede ser 0.', notZeros),
    },
    autorizado: { 
        required: helpers.withMessage('Campo requerido', required) ,
        SoloLetras: helpers.withMessage('Solo puede introducir caracteres alfanuméricos.', SoloLetras) ,
        maxLength:  helpers.withMessage('La longitud máxima permitida es de 50 caracteres.', maxLength(50)) 
    }
}
