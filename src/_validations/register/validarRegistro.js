import { required, maxLength, minLength, helpers } from "@vuelidate/validators";
import { notZeros,notSeleccione, SoloNumeros, numberAndDecimals,SoloAlfanumericos,onlyLettersSpecial /* onlyNumber_espec */ } from "@/_validations/ValidacionEspeciales"; 

// export const validarRegistro = {
//     producto: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
//     sorteo: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
//     tipoPago: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
//     ctaDestino_caja: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
//     banco_oficina: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },   
//     fecha: { required },
//     monto: { 
//         required: helpers.withMessage('Campo requerido', required), 
//         numberAndDecimals: helpers.withMessage('Solo puede introducir coma (,), seguido de máximo 2 dígitos. Ej: 0,00', numberAndDecimals), 
//         maxLength:helpers.withMessage('La longitud máxima permitida es de 11 carácteres.', maxLength(11)) ,
//     },
//     referencia: { 
//         SoloNumeros: helpers.withMessage('Solo puede introducir números.', SoloNumeros) ,
//         maxLength:  helpers.withMessage('La longitud máxima permitida es de 20 carácteres.', maxLength(20)) ,
//     }
// }

export const validarRegistro = (typeFilter = '') => {
  // console.log('typeFilter',typeFilter);
   /*'MDk=': C2P
    'MDM=' : PAGO MÓVIL
    'MDY=' : DEPÓSITO
    'MTA=' : TDC
    'MDI=': TRANSFERENCIA
  */
 const valModel = {
  producto: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
    medioPago: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
    sorteo: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
    // tipoPago: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
    // ctaDestino_caja: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
    typeDocuments: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
    cedula : {required: helpers.withMessage('Campo requerido', required)},
    referencia : {required: helpers.withMessage('Campo requerido', required)},
    telefono : {required: helpers.withMessage('Campo requerido', required)},
    monto: { 
        required: helpers.withMessage('Campo requerido', required), 
        numberAndDecimals: helpers.withMessage('Solo puede introducir coma (,), seguido de máximo 2 dígitos. Ej: 0,00', numberAndDecimals), 
        maxLength:helpers.withMessage('La longitud máxima permitida es de 7 caracteres.', maxLength(7)) ,
        notZeros: helpers.withMessage('El monto no puede ser 0.', notZeros),
    },
    ...(typeFilter === 'MTA=' || typeFilter === 'MDY='? {} : { 
      banco_oficina: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },  
      }),
      ...(typeFilter === 'MDY=' ? {
        moneda: { notSeleccione: helpers.withMessage('Campo requerido.', notSeleccione) },
      } : {}),
 }
 
  // console.log('valModel desde validation', valModel);

  return valModel;
};
