import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import { useMainStore } from "@/stores/main.js";
import { useStyleStore } from "@/stores/style.js";
import { darkModeKey, styleKey } from "@/config.js";
import { useSesionStore } from "@/stores/mSesion.js";
import piniaPersist from 'pinia-plugin-persist'
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';


import "./css/main.css";

/* Init Pinia */
const pinia = createPinia();
pinia.use(piniaPersist)

/* Create Vue app */
createApp(App).use(router).use(pinia).use(VueLoading).mount("#app");

/* Init Pinia stores */
// const mainStore = useMainStore(pinia);
const styleStore = useStyleStore(pinia);
const mSesion = useSesionStore(pinia);

/* Fetch sample data */
// mainStore.fetch("clients");
// mainStore.fetch("history");
mSesion.loginSystem();


/* App style */
styleStore.setStyle(localStorage[styleKey] ?? "basic");

/* Dark mode */
if (
  (!localStorage[darkModeKey] &&
    window.matchMedia("(prefers-color-scheme: dark)").matches) ||
  localStorage[darkModeKey] === "1"
) {
  styleStore.setDarkMode(true);
}

/* Default title tag */
const defaultDocumentTitle = "tg-kg-cobranza";

/* Set document title from route meta */
router.afterEach((to) => {
  document.title = to.meta?.title
    ? `${to.meta.title} — ${defaultDocumentTitle}`
    : defaultDocumentTitle;
});

