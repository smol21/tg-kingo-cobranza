import { createRouter, createWebHashHistory } from "vue-router";
import Style from "@/views/StyleView.vue";
import Home from "@/views/HomeView.vue";
import Login from "@/views/LoginView.vue";
import RegisterPay from "@/views/RegisterPay/registerpay.vue"
import { useSesionStore } from "@/stores/mSesion.js";
const routes = [
  {
    meta: {
      title: "Login",
    },
    path: "/",
    name: "login",
    component: Login,
  },
  {
    // Document title tag
    // We combine it with defaultDocumentTitle set in `src/main.js` on router.afterEach hook
    meta: {
      title: "Dashboard",
       requiresAuth: true
    },
    path: "/dashboard",
    name: "dashboard",
    component: Home,
  },
  {
    meta:{
      title:"Registrar Pago",
      requiresAuth: true
    },
    path:"/pago",
    name:"Registrar Pago",
    component: () => import("@/views/RegisterPay/registerPay.vue"),

  },
  {
    meta:{
      title:"Registrar Descuento",
      requiresAuth: true
    },
    path:"/descuento",
    name:"Registrar Descuento",
    component: () => import("@/views/RegisterDiscount/registerDiscount.vue"),

  },
  {
    meta:{
      title:"Gestionar Movimiento",
      requiresAuth: true
    },
    path:"/gestionarMovimiento",
    name:"Gestionar Movimiento",
    component: () => import("@/views/GestionarMovimiento/gestionarMovimiento.vue"),

  },
  {
    meta: {
      title: "Tables",
    },
    path: "/tables",
    name: "tables",
    component: () => import("@/views/TablesView.vue"),
  },
  {
    meta: {
      title: "Forms",
    },
    path: "/forms",
    name: "forms",
    component: () => import("@/views/FormsView.vue"),
  },
  {
    meta: {
      title: "Profile",
    },
    path: "/profile",
    name: "profile",
    component: () => import("@/views/ProfileView.vue"),
  },
  {
    meta: {
      title: "Ui",
    },
    path: "/ui",
    name: "ui",
    component: () => import("@/views/UiView.vue"),
  },
  {
    meta: {
      title: "Responsive layout",
    },
    path: "/responsive",
    name: "responsive",
    component: () => import("@/views/ResponsiveView.vue"),
  },
  // {
  //   meta: {
  //     title: "Login",
  //   },
  //   path: "/login",
  //   name: "login",
  //   component: () => import("@/views/LoginView.vue"),
  // },
  {
    meta: {
      title: "Error",
    },
    path: "/error",
    name: "error",
    component: () => import("@/views/ErrorView.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { top: 0 };
  },
});

router.beforeEach((to, from, next) => {
  const aux = useSesionStore();
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (aux.data_user.userToken) {
      next();
    } else {
      next({ name: "login" });
    }
  } else {
    next();
  }
});

export default router;
