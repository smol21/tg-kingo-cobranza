import { defineStore } from "pinia";
import { API_COBRANZA, API_GENERAL } from "../api/urlBack.js";
import { useSesionStore } from "@/stores/mSesion.js";
import { fireToastMsg } from "../_globals/global.js";
import { useLoading } from "vue-loading-overlay";

export const useGeneralStore = defineStore("general", {
  state: () => ({
    slistProduct: [],
    slistCC: [],
    slistSorteo: [],
    slistTipoPagoRegPay: [],
    slistTipoPagoMov: [],
    slistBancoRegPay: [],
    slistBanco: [],
    slistCtaCajaRegPay: [],
    slistCtaCaja: [],
    slistMediopago:[],
    slistMoneda: [],
    slistTipoCuenta: [],
    slistTipoTarjeta:[]
  }),
  actions: {
    // login sistema
    //LISTAR PRODUCTO
    async listProduct() {
      this.slistProduct = [];
      const tokenSistema = useSesionStore();
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      fetch(`${API_GENERAL}ListarProducto/MDM=/MDAwMA==/MQ==`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
        .then((json) => {
          if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
            const temp = [{code: null, description:'SELECCIONE'}, ...json.datos]
            this.slistProduct = temp;
            loader.hide();
          }else{
            const errorMsg = json.message.description || json?.mensaje?.description;
            fireToastMsg(errorMsg);
            loader.hide();
          }
        })
        .catch((error) => {
          console.log("error",error)
          const msg = "Solicitud no procesada.";
          fireToastMsg(msg);
          loader.hide();
        });
    },
    //LISTAR MONEDA
    async listMoneda(codMedio) {
      this.slistMoneda = [];
      const tokenSistema = useSesionStore();
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      fetch(`${API_COBRANZA}ListarMoneda/` + codMedio + '/MDA=', {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
        .then((json) => {
          if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
            const temp = [{code: null, name:'SELECCIONE'}, ...json.datos]
            this.slistMoneda = temp;
            loader.hide();
          }else{
            const errorMsg = json.message.description || json?.mensaje?.description;
            fireToastMsg(errorMsg);
            loader.hide();
          }
        })
        .catch((error) => {
          console.log("error",error)
          const msg = "Solicitud no procesada.";
          fireToastMsg(msg);
          loader.hide();
        });
    },
    //LISTAR COMPRADOR COMERCIAL
    async listCC() {
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      this.slistCC = [];
      const tokenSistema = useSesionStore();
      fetch(`${API_GENERAL}ListarCompradorComercial/MDM=/MDAwMDAw`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          const temp = [{code: null, name:'SELECCIONE'}, ...json.datos]
          this.slistCC = temp;
          loader.hide();
        }else{
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error", error);
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg);
        loader.hide();
      });
    },
    async listSorteo(val) {
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      this.slistSorteo = [];
      if( val === 'XXX'){
        const temp = [{code: null, name:'SELECCIONE'}]
        this.slistSorteo = temp;
        loader.hide();
      }else{
      const tokenSistema = useSesionStore();
      fetch(`${API_GENERAL}ListarSorteoAdm/MDM=/` + val + `/MDAw`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          const temp = [{code: null, name:'SELECCIONE'}, ...json.datos]
          this.slistSorteo = temp;
          loader.hide();
        }
        else{
          const temp = [{code: null, name:'SELECCIONE'}]
          this.slistSorteo = temp;
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error", error);
        loader.hide();
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg);
      });
    }
    },
    // async listTipoPagoMov(val) {
    //   this.slistTipoPagoMov = [];
    //   const tokenSistema = useSesionStore();
    //   const $loading = useLoading();
    //   const loader = $loading.show({
    //     loader: "dots",
    //     color: "#0C7F08",
    //     width: 100,
    //     height: 100,
    //   });
    //   fetch(`${API_COBRANZA}ListarTipoPagoMovimiento/MDM=/MDA=/` + val, {
    //     headers: {
    //       "Content-Type": "application/json",
    //       Authorization: "Bearer " + tokenSistema.sesion_data.token,
    //     },
    //   })
    //   .then((response) => response.json())
    //   .then((json) => {
    //     if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
    //     const temp = [{code:  window.btoa("00"), name:'SELECCIONE'}, ...json.datos]
    //     const temp_pago = [{code: null, name:'SELECCIONE'}, ...json.datos]
    //       this.slistTipoPagoMov = temp;
    //       this.slistTipoPagoRegPay = temp_pago;
    //       loader.hide();
    //     }
    //     else{
    //       const temp = [{code: null, name:'SELECCIONE'}]
    //       this.slistTipoPagoMov = temp;
    //       this.slistTipoPagoRegPay = temp;
    //       const errorMsg = json.message.description || json?.mensaje?.description;
    //       fireToastMsg(errorMsg);
    //       loader.hide();
    //     }
    //   })
    //   .catch((error) => {
    //     console.log("error tipo PAGO", error);
    //     loader.hide();
    //     const msg = "Solicitud no procesada.";
    //     fireToastMsg(msg);
    //   });
    // },
    async listBanco(val) {
      this.slistBanco = [];
      const tokenSistema = useSesionStore();
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      if( val === 'XXX' || val === null ){
        const temp = [{code: null, name:'SELECCIONE'}]
        this.slistBanco = temp;
        loader.hide();
      }else{
      fetch(`${API_COBRANZA}ListarBancOficina/MDM=/` + val + `/MDAw/Mg==`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
    })
    .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          // const temp = [{code: window.btoa("000"), name:'SELECCIONE'}, ...json.datos]
          const temp = [{code: null, name:'SELECCIONE'}, ...json.datos]
          // const temp_banco = [{code: null, name:'SELECCIONE'}, ...json.datos]
          this.slistBanco = temp;
          // this.slistBancoRegPay = temp_banco;
          loader.hide();
        }
        else{
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error banco", error);
        loader.hide();
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg);
      });
    }
    },
    async listCtaCaja(val) {
      this.slistCtaCaja = [];
      const tokenSistema = useSesionStore();
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      if( val === 'XXX' || val === null){
        const temp = [{code: null, name:'SELECCIONE'}]
        this.slistCtaCaja = temp;
        loader.hide();
      }else{
      fetch(`${API_COBRANZA}ListarCtaCajaOperadora/MDM=/` + val, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          // const temp = [{code: window.btoa("0"), name:'SELECCIONE'}, ...json.datos]
          const temp = [{code: null, name:'SELECCIONE'}, ...json.datos]
          // const temp_cta_caja = [{code: null, name:'SELECCIONE'}, ...json.datos]
          this.slistCtaCaja = temp;
          // this.slistCtaCajaRegPay = temp_cta_caja;
          loader.hide();
        }
        else{
          const temp = [{code: null, name:'SELECCIONE'}]
          this.slistCtaCaja = temp;
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error",error)
        loader.hide();
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg);
      });
      loader.hide();
    }
    },
    /* Funcion que lista los Medio de Pago
    ** Agg '000' si deseas listar todos los medios    **
    ** Agg el tipo de cuenta para filtrar encriptado  **
    */

    // ListarMedioPago/{codSoftware}/{tipoCuenta}/{codigoMedio}/{tipOpc} 
    // tipOpc = 0: si viene de Registro de abono, 1: si viene del pagar. 3: si viene de cobranza registra pago 4: cobranza Gestión movimiento, todos encriptados
    

    
    async listMedioPago(val) {
      this.slistMediopago = [];
      if(val !== 'Mg==' && val === null && val !== 'Mw=='){
        const temp = [{code: null, name:'SELECCIONE'}]
        this.slistMediopago = temp;
      }else{  
      const tokenSistema = useSesionStore();
      fetch(`${API_COBRANZA}ListarMedioPago/MDM=/MDA=/MDA=/${val}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          const temp = [{code: null, name:'SELECCIONE'}, ...json.datos]
          this.slistMediopago = temp;
        }
        else{
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error cargando medio de pago", error); 
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg);
      });
      }
    },

     /* Función que lista los tipo de cuenta solo para el medio de pago TDC*/
    async listarTipoCuenta() {
      this.slistTipoCuenta = [];
      const tokenSistema = useSesionStore();
      fetch(`${API_COBRANZA}ListarTipoCuenta/MDE=`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          const temp = json.datos.map((dto) => {
            return { name: `${dto.valor}`, code: dto.codigo };
          });
          this.slistTipoCuenta = [{ name: 'SELECCIONE', code: null }, ...temp];
        }
        else{
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error cargando tipo cuenta para TDC", error);
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg); 
      });
    },
    /* Función que lista los tipo de tarjeta solo para el medio de pago TDC*/
    async listarTipoTarjeta() {
      this.slistTipoCuenta = [];
      const tokenSistema = useSesionStore();
      fetch(`${API_COBRANZA}ListarTipoTarjeta/MDE=`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if(json?.mensaje?.code === '000' || json?.message?.code === '000'){
          const temp = json.datos.map((dto) => {
            return { name: `${dto.valor}`, code: dto.codigo };
          });
          this.slistTipoTarjeta = [{ name: 'SELECCIONE', code: null }, ...temp];
        }
        else{
          const errorMsg = json.message.description || json?.mensaje?.description;
          fireToastMsg(errorMsg);
          loader.hide();
        }
      })
      .catch((error) => {
        console.log("error cargando tipo tarjeta para TDC", error); 
        const msg = "Solicitud no procesada.";
        fireToastMsg(msg);
      });
    },
  },
});


