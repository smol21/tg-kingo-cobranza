import { defineStore } from "pinia";
import { API_COBRANZA } from "../api/urlBack.js";
import { useSesionStore } from "@/stores/mSesion.js";
import { format_epoch, DateFormaterEpoch } from "@/_globals/global.js";
import { fireToastMsg } from "../_globals/global.js";
import { useLoading } from "vue-loading-overlay";
import router from "../router/index";

export const useReporteStore = defineStore("reporte", {
  state: () => ({
    item_table: [],
  }),
  actions: {
    async listarMovimientos(obj) {
      const $loading = useLoading();    
      let loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });

      let tipoMov = obj.tipoMov.code;
      let cuenta = obj.cuenta.code;
      let banco_oficina = obj.banco_oficina.code;
      //cuando es descuento
      if(obj.tipoMov.code === 'MDg=' || obj.tipoMov.code === null || ((obj.tipoMov.code != 'MDg=') && (obj.tipoMov.code != null) && obj.cuenta.code === null && obj.banco_oficina.code === null)){
        tipoMov = 'MDA=';
        cuenta = 'MA==';
        banco_oficina = 'MDAw';
        //cuando es seleccione
      }else if((obj.tipoMov.code != 'MDg=') && (obj.tipoMov.code != null) && obj.cuenta.code != null && obj.banco_oficina.code === null){
        cuenta = obj.cuenta.code;
        banco_oficina = 'MDAw';
      }else if((obj.tipoMov.code != 'MDg=') && (obj.tipoMov.code != null) && obj.cuenta.code === null && obj.banco_oficina.code != null){
        cuenta = 'MA==';
        banco_oficina = obj.banco_oficina.code;
      }
      
      const date_desde = format_epoch(obj.fecha_desde);
      const date_hasta = format_epoch(obj.fecha_hasta);
      this.item_table = [];
      const tokenSistema = useSesionStore();
      const storeData = useSesionStore();
      const code_cc = storeData.data_user.userType === '05' ? 'MDAwMDAw' : obj.comprador.code
      fetch(
        `${API_COBRANZA}ListarMovimientos/${tokenSistema.data_user.userToken}/MDM=/${code_cc}/${obj.producto.code}/${obj.sorteo.code}/${date_desde}/${date_hasta}/${tipoMov}/${cuenta}/${banco_oficina}/${obj.estatus.code}`,
        {
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: "Bearer " + tokenSistema.sesion_data.token,
          },
        }
      )
        .then((response) => response.json())
        .then((json) => {
          if (json.datos) {
            const listado = [...json.datos];
            this.item_table = listado.map((listado) =>
              Object.assign({}, this.item_table, {
                Comprador: listado.nombreComprador,
                Tipo_De_Movimiento: listado.nombreMedioPago,
                Concepto: listado.descripcionMov,
                Fecha: DateFormaterEpoch(listado.fechaMovimiento),
                Banco: listado.banco,
                Referencia_de_Movimiento: +window.atob(listado.referencia),
                Monto: listado.montoMovimiento,
                Autorizado_por: listado.aprobadoPor,
                // codigoMovimiento: window.atob(listado.codigoMovimiento),
                // estatusMovimiento: listado.estatusMovimiento
              })
            );
          } else if (json.message.code === '008') {
            const errorMsg = json.message.description;
            fireToastMsg(errorMsg);
            storeData.menu_list = [];
            storeData.data_user = [];
            router.push("/");
          } 
          else {
            const errorMsg = json.message.description;
            fireToastMsg(errorMsg);
          }
          loader.hide();
        })
        .catch((err) => {
          const errorMsg =
            err?.response?.data?.message?.description ||
            err?.responseJSON?.message?.description ||
            err?.responseJSON?.mensaje?.description ||
            err?.response?.statusText ||
            err?.message?.description ||
            err.message.description ||
            "";
          fireToastMsg(errorMsg);
          loader.hide();
        });
    },
  },
  persist: {
    enabled: true,
  },
});
