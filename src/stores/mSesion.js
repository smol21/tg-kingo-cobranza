import { defineStore } from "pinia";
import { API_GENERAL , API_USUARIO } from "../api/urlBack.js";
import router from "../router/index";
import { fireToastMsg } from "../_globals/global.js";
import { useLoading } from "vue-loading-overlay";
import { useLocalStorage } from "@vueuse/core";

export const useSesionStore = defineStore("sesion", {
  state: () => ({
    sesion_data: useLocalStorage('sesion_data',[]),
    menu_list: useLocalStorage('menu_list',[]),
    data_user: "",
  }),

  actions: {
    // login sistema
    async loginSystem() {
      const login_temp = "sistemacobranza",
        password_temp = "abc.123",
        url_temp = "sistemacobranza.com.ve",
        A1 = window.btoa(login_temp),
        A2 = window.btoa(password_temp),
        A3 = window.btoa(url_temp);
      fetch(`${API_GENERAL}IniciarSesion`, {
        method: "POST",
        body: JSON.stringify({
          A1: A1,
          A2: A2,
          A3: A3,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      })
        .then((response) => response.json())
        .then((json) => {
          this.sesion_data = json;
        })
        .catch((error) => {
          console.log("error", error);
        });
    },
    async inicioSesion(login, pass) {
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      fetch(`${API_USUARIO}IniciarSesion`, {
        method: "POST",
        body: JSON.stringify({
          login: login,
          password: window.btoa(pass),
          appCode: "MDM=",
          id: "00000000000000000000000000000000",
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: "Bearer " + this.sesion_data.token,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          if (json.message.code && json.message.code === "000") {
            this.menu_list = json.data.menuList;
            this.data_user = json.data.userData;
            router.push("/dashboard");

            const welcomeMsg = `Bienvenido: ${this.data_user.userName}`;
            fireToastMsg(welcomeMsg, "success");
          } else {
            const errorMsg = `${json.message.description}`;
            fireToastMsg(errorMsg);
          }
          loader.hide();
        })
        .catch((error) => {
          console.error("error versionApp", error);
          const errorMsg = "Error General de la aplicación.";
          fireToastMsg(errorMsg);
          loader.hide();
        });
    },
    async sessionLogout() {
      const id = "00000000000000000000000000000000";
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      if (this.data_user.userToken) {
        fetch(`${API_USUARIO}CerrarSesion/${this.data_user.userToken}/${id}`, {
          method: "delete",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: "Bearer " + this.sesion_data.token,
          },
        })
          .then((response) => response.json())
          .then((json) => {
            if (json.message.code && json.message.code === "000") {
              const closeMsg = `Sesión finalizada`;
              fireToastMsg(closeMsg, "success");
              this.menu_list = [];
              this.data_user = [];
              router.push("/");
            } else {
              const errorMsg = `${json.message.description}`;
              fireToastMsg(errorMsg);
              this.menu_list = [];
              this.data_user = [];
              router.push("/");
            }
            loader.hide();
          })
          .catch((error) => {
            console.error("error", error);
            loader.hide();
          });
      }
    },
  },
  persist: {
    enabled: true,
  },
});
