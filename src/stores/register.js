import { defineStore } from "pinia";
import { API_COBRANZA } from "../api/urlBack.js";
import { useSesionStore } from "@/stores/mSesion.js";
import { fireToastMsg, format_epoch } from "../_globals/global.js";
import { useLoading } from "vue-loading-overlay";
import router from "../router/index";
import Swal from "sweetalert2";

export const useRegisterStore = defineStore("register", {
  state: () => ({}),
  actions: {
    async registerDiscount(form) {
      const codigoComprador_ = form.comprador.code;
      const descripcion_ = form.descripcion;
      const codigoProducto_ = form.producto.code;
      const montoMovimiento_ = form.monto;
      const numeroSorteo_ = form.sorteo.code;
      const autorizadoPor_ = form.autorizado;
      const fechaMovimiento_ = format_epoch(form.fecha);

      const montoReplace = montoMovimiento_.replace(",", ".");

      const tokenSistema = useSesionStore();
      const storeData = useSesionStore();

      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });

      fetch(`${API_COBRANZA}RegistrarDescuento`, {
        method: "POST",
        body: JSON.stringify({
          codigoAplicacion: "MDM=", //Enviar dato encriptado
          codigoComprador: codigoComprador_, //Enviar dato encriptado
          concepto: descripcion_,
          codigoProducto: codigoProducto_, //Enviar dato encriptado
          fechaMovimiento: fechaMovimiento_, //Enviar dato en formato epoch
          montoMovimiento: montoReplace,
          numeroSorteo: numeroSorteo_, //Enviar dato encriptado
          autorizadoPor: autorizadoPor_,
          tokenUsuario: tokenSistema.data_user.userToken,
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          if (json?.mensaje?.code === "000" || json?.message?.code === "000") {
            const msnExito = json.message.description;
            fireToastMsg(msnExito, "success");
          } else if (
            json?.mensaje?.code === "008" ||
            json?.message?.code === "008"
          ) {
            const msnError = json.message.description;
            fireToastMsg(msnError, "warning");
            storeData.menu_list = [];
            storeData.data_user = [];
            router.push("/");
          } else {
            const msnError = json.message.description;
            fireToastMsg(msnError, "warning");
          }
          loader.hide();
        });
    },
    async registerPay(form) {
      const banco_oficina_ = form.banco_oficina.code;
      const ctaDestino_caja_ = form.ctaDestino_caja.code;
      const fechaPago_ = format_epoch(form.fecha);
      const montoPago_ = form.monto;
      const producto_ = form.producto.code;
      const referencia_ = form.tipoPago.code === 'MDc=' ? '00000000000000000000' : form.referencia;
      const numeroSorteo_ = form.sorteo.code;
      const tipoPago_ = form.medioPago.code;
      const montoReplace = montoPago_.replace(",", ".");
      const ref_encriptada = referencia_ ? window.btoa(referencia_) : null;
      const tipo_cuenta = form.tipo_cuenta.code ? window.btoa(form.tipo_cuenta.code) : form.tipo_cuenta.code;
      const tipo_tarjeta = form.tipo_tarjeta.code ? window.btoa(form.tipo_tarjeta.code) : form.tipo_tarjeta.code;
      const nro_tarjeta =  window.btoa(form.nro_tarjeta);
      const fecha_expiracion =  window.btoa(form.fecha_expiracion);
      const nombre_titular = form.nombre_titular;
      const cvv =  window.btoa(form.cvv);
      const clave_pin =  window.btoa(form.clave_pin);

      const typeDocuments = form.typeDocuments.name;
      const cedula = typeDocuments + form.cedula;
      const cedula_encriptada = window.btoa(cedula);

      const telefono = form.telefono ? form.telefono : null;
      const moneda = form.moneda.code;

      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });

      const tokenSistema = useSesionStore();
      const storeData = useSesionStore();
      
      fetch(`${API_COBRANZA}RegistrarPago`, {
        method: "POST",
        body: JSON.stringify({
          codigoAplicacion: "MDM=", //Enviar dato encriptado
          codigoBanco: banco_oficina_, //Enviar dato encriptado
          codigoCuenta: ctaDestino_caja_, //Enviar dato encriptado
          codigoMoneda:moneda,
          codigoProducto: producto_, //Enviar dato encriptado
          fechaMovimiento: fechaPago_, //Enviar dato en formato epoch
          montoMovimiento: montoReplace, //Enviar dato encriptado
          numeroReferencia: ref_encriptada,
          numeroSorteo: numeroSorteo_, //Enviar dato encriptado
          tipoMovimiento: tipoPago_, //Enviar dato encriptado
          tokenUsuario: tokenSistema.data_user.userToken,
          telefono:telefono,
          cedula: cedula_encriptada,
          tipoTarjeta: tipo_tarjeta,
          numeroTarjeta: nro_tarjeta,
          fecExpiracion: fecha_expiracion,
          nombreTitular: nombre_titular,
          tipoCuenta:tipo_cuenta,
          CVV: cvv,
          clavePin: clave_pin,
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + tokenSistema.sesion_data.token,
        },
      })
        .then((response) => response.json())
        .then((json) => {
          const msnExito = json.message.description;
          if (json?.mensaje?.code === "000" || json?.message?.code === "000") {
            fireToastMsg(msnExito, "success");
          } else if (
            json?.mensaje?.code === "008" ||
            json?.message?.code === "008"
          ) {
            const msnError = json.message.description;
            fireToastMsg(msnError, "warning");
            storeData.menu_list = [];
            storeData.data_user = [];
            router.push("/");
          } else {
            const msnError = json.message.description;
            fireToastMsg(msnError, "warning");
          }
          loader.hide();
        });
    },
    async registerMovement(data, btn, observacionMovimiento) {
      const $loading = useLoading();
      const loader = $loading.show({
        loader: "dots",
        color: "#0C7F08",
        width: 100,
        height: 100,
      });
      const token = useSesionStore();
      let confExt = 0;
      let confRzo = 0;
      const promesas = data.value.map(async (checkedName) => {
        try {
          const codMov = checkedName.codigoMovimiento;
          const resp = await fetch(`${API_COBRANZA}GestionarMovimiento`, {
            method: "PUT",
            body: JSON.stringify({
              codigoAplicacion: "MDM=", //Enviar dato encriptado
              codigoMovimiento: window.btoa(codMov),
              tokenUsuario: token.data_user.userToken,
              estatusMovimiento: btn,
              observacionMovimiento 
            }),
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + token.sesion_data.token,
            },
          });
          return resp;
        } catch (error) {
          console.log("error", error);
          
          return false;
        }
      });
      await Promise.allSettled(promesas).then((results) => results.forEach((result) => {
        if(result.value.ok === true || result.value.status === '200') {
          confExt = confExt + 1;
         } else {
          confRzo = confRzo + 1
         };
      }));
      loader.hide();
      Swal.fire({
        html: 'Operaciones exitosas: ' + confExt + '<br>' + 'Operaciones no exitosas: ' + confRzo,
        title: btn == 1 ? 'Movimientos Procesados' : 'Movimientos Rechazados',
        confirmButtonText: 'OK',
        confirmButtonColor: '#3085d6',
      });
    },
  },
});
